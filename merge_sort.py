import random 

def mergesort(arr):
    if len(arr) > 1:
        mid = len(arr)/2
        left = arr[:mid]
        right = arr[mid:]

        left = mergesort(left)
        right = mergesort(right)

        i = j = k = 0
        
        left_len = len(left)
        right_len = len(right)

        while i < left_len and j < right_len:
            if left[i] < right[j]:
                arr[k] = left[i]
                i += 1
            else:
                arr[k] = right[j]
                j += 1
            k += 1

        while i < left_len:
            arr[k] = left[i]
            i = i + 1
            k = k + 1
        while j < right_len:
            arr[k] = right[j]
            j = j + 1
            k = k + 1
    return arr


def main():
    array = [5,6,4,9,31,403, 30,44,205, 64,29,49,70,50,103,130]
    print array
    array = mergesort(array)
    print array
    print '-'*20
    array = [random.randint(0,1000) for _ in xrange(30)]
    print array
    array = mergesort(array)
    print array
    
if __name__ == '__main__':
    main()
